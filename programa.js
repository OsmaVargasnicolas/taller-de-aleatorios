function arreglosAleatorios ()
{
    let a = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    a[0]=Math.random()*100;
    a[1]=Math.random()*100;
    a[2]=Math.random()*100;
    a[3]=Math.random()*100;
    a[4]=Math.random()*100;
    a[5]=Math.random()*100;
    a[6]=Math.random()*100;
    a[7]=Math.random()*100;
    a[8]=Math.random()*100;
    a[9]=Math.random()*100;

    a[0]=Math.round(a[0]);
    a[1]=Math.round(a[1]);
    a[2]=Math.round(a[2]);
    a[3]=Math.round(a[3]);
    a[4]=Math.round(a[4]);
    a[5]=Math.round(a[5]);
    a[6]=Math.round(a[6]);
    a[7]=Math.round(a[7]);
    a[8]=Math.round(a[8]);
    a[9]=Math.round(a[9]);

    let n= a.length;
    console.log(a); // Mostramos, por consola, la lista desordenada
    // Algoritmo de burbuja
    for (let k = 1; k < n; k++) {
        for (let i = 0; i < (n - k); i++) {
            if (a[i] > a[i + 1]) 
            {
                let aux = a[i];
                a[i] = a[i + 1];
                a[i + 1] = aux;
            }
        }
    }

    console.log(a); // Mostramos, por consola, la lista ya ordenada

}

